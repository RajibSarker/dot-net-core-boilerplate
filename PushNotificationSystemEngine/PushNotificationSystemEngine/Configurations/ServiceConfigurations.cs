﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PushNotificationSystemEngine.Database_Context;

namespace PushNotificationSystemEngine.Configurations
{
    public class ServiceConfigurations
    {
        public static void Configuration(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ApplicationDbContext>(opts =>
                opts.UseNpgsql(configuration.GetConnectionString("AppConnectionString")
            //npgsqlOptionsAction: sqlOptions =>
            //{
            //    sqlOptions.EnableRetryOnFailure();
            //}
            ));
        }
    }
}
